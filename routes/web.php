<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return "Silent Is Golden :)";
});

$app->get('/users','UserController@index');

/**
 * Routes using auth middleware (Login User Access Token)
 */
$app->post('/login', 'LoginController@index');
$app->post('/register', 'DatasController@register');
$app->get('/datas/{id}', ['middleware' => 'auth', 'uses' => 'DatasController@get_data']);

/**
 * Routes using auth middleware (CRUD Category)
 */
$app->get('/category', 'CategoryAdsController@index');
$app->get('/category/{id}', 'CategoryAdsController@read');
$app->post('/category', 'CategoryAdsController@create');
$app->put('/category/update/{id}', 'CategoryAdsController@update');
$app->delete('/category/delete/{id}', 'CategoryAdsController@delete');

/**
 * Routes using auth middleware (CRUD Item Ads)
 */
$app->get('/item_ads', 'ItemAdsController@index');
$app->get('/item_ads/{id}', 'ItemAdsController@read');
$app->post('/item_ads/create', 'ItemAdsController@create');
$app->put('/item_ads/update/{id}', 'ItemAdsController@update');
$app->delete('/item_ads/delete/{id}', 'ItemAdsController@delete');