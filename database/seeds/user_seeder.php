<?php

use Illuminate\Database\Seeder;

class user_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        for ($i=0; $i < 100; $i++) 
        { 
        	DB::table('users')->insert([
				'user_name'       => $faker->name,
				'user_email'      => $faker->email,
				'user_address'    => $faker->address,
				'user_created_at' => date('Y-m-d H:i:s'),
				'user_updated_at' => date('Y-m-d H:i:s')
        	]);
        }
    }
}
