<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('datas')->delete();
		$item      = app()->make('App\Libraries\Datas');
		$hasher    = app()->make('hash');
		$password  = $hasher->make('password');
		$api_token = sha1(time());
		
        $item->fill([
			'username'  => 'Ahmad Rosid',
			'email'     => 'ocittwo@gmail.com',
			'password'  => $password,
			'api_token' => $api_token
        ]);

        $item->save();
    }
}
