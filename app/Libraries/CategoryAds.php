<?php
	namespace App\Libraries;

	use Illuminate\Database\Eloquent\Model;
	/**
	* 
	*/
	class CategoryAds extends Model
	{
		protected $table    = 'category_ads';
		protected $fillable = ['name'];
	}