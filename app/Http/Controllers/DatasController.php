<?php
	namespace App\Http\Controllers;
	
	use Illuminate\Http\Request;
	use App\Libraries\Datas;

	class DatasController extends Controller
	{
		public function register(Request $request)
		{
			$hasher   = app()->make('hash');
			$username = $request->input('username');
			$email    = $request->input('email');
			$password = $hasher->make($request->input('password'));
			
			$register = Datas::create([
				'username' => $username,
				'email'    => $email,
				'password' => $password,
			]);

			if ($register) 
			{
				$res['success'] = true;
				$res['message'] = 'Success register!';

				return response($res);
			} 
			else 
			{
				$res['success'] = false;
				$res['message'] = 'Failed to register!';
			
				return response($res);
			}
		}

		public function get_data(Request $request, $id)
		{
			$user = Datas::where('id', $id)->get();

			if ($user) {
				$res['success'] = true;
				$res['message'] = $user;

				return response($res);
			} else {
				$res['success'] = false;
				$res['message'] = 'Cannot find user!';

				return response($res);
			}
		}
	}