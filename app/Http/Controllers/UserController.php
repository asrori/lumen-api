<?php

namespace App\Http\Controllers;
use App\Libraries\Users;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Users $users)
    {
    	$user = $users->AllUsers();
    	return response()->json($user);
    }
}
